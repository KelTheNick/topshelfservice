﻿using System;
using Topshelf;

namespace TopshelfService
{
    class Program
    {
        public static void Main(string[] args)
        {
            HostFactory.New(hostConfig =>                                   
            {
                hostConfig.Service<ServiceCtrl>();

                hostConfig.SetDescription("Sample Topshelf Service");
                hostConfig.SetDisplayName("Sample Topshelf Service");
                hostConfig.SetServiceName("Sample Topshelf Service");

                hostConfig.RunAsLocalSystem();                               
            });    
            
        }
    }
}
